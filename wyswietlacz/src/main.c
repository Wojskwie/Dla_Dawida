/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f7xx.h"
#include "stm32746g_discovery.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"
#include <stdio.h>

#define LCD_FRAME_BUFFER          SDRAM_DEVICE_ADDR
#define RGB565_BYTE_PER_PIXEL     2
#define ARBG8888_BYTE_PER_PIXEL   4

static TS_StateTypeDef  TS_State;

static void SystemClock_Config(void);
static void Error_Handler(void);
static void MPU_Config(void);
static void CPU_CACHE_Enable(void);
int x = 0;
int y = 0;
short choice = 0;
uint8_t relay = 0;


int main(void)
{
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;
  GPIOI->PUPDR |= GPIO_PUPDR_PUPDR11_0;

  MPU_Config();
  CPU_CACHE_Enable();
  HAL_Init();
  SystemClock_Config();

  BSP_LCD_Init();
  BSP_LCD_LayerDefaultInit(LTDC_ACTIVE_LAYER, LCD_FRAME_BUFFER);
  BSP_LCD_SetLayerVisible(LTDC_ACTIVE_LAYER, ENABLE);
  BSP_LCD_SelectLayer(LTDC_ACTIVE_LAYER);
  BSP_LCD_Clear(LCD_COLOR_GREEN);
  BSP_LCD_SetBackColor(LCD_COLOR_GREEN);
  BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
  BSP_LCD_DisplayOn();
  BSP_LCD_SetFont(&Font24);

  int status = BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
  if(status !=TS_OK)
  {
	  BSP_LCD_DrawRect(60, 200,  100, 40 );
  }

  void drawMainMenu()
  {
	  char title[25];
	  sprintf(title,"System Zdalnego Nadzoru");
	  BSP_LCD_DisplayStringAtLine(0, (uint8_t *) title);

	  char options[40];
	  sprintf(options,"Temp   Obecnosc   Relay");
	  BSP_LCD_DisplayStringAtLine(3, (uint8_t *) options);

	  uint8_t flag = 0;
	  if(GPIOI->IDR & GPIO_IDR_IDR_11) flag = 1;
	  if(flag) BSP_LCD_FillCircle(180,130,20);
	  else
	  {
		  BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
		  BSP_LCD_FillCircle(180,130,20);
		  BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
		  BSP_LCD_DrawCircle(180,130,20);
	  }

	  if(relay) BSP_LCD_FillCircle(350,130,20);
	  	  else
	  	  {
	  		  BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
	  		  BSP_LCD_FillCircle(350,130,20);
	  		  BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
	  		  BSP_LCD_DrawCircle(350,130,20);
	  	  }

	  char state[10];
	  sprintf(state,"22,50");
	  BSP_LCD_DisplayStringAtLine(5, (uint8_t *) state);




  }
  /*void drawTempMenu()
  {
	  char buffer[30];
	  sprintf(buffer,"Temperatury");
	  BSP_LCD_DisplayStringAtLine(1, (uint8_t *) buffer);
  }*/
  /*void drawMenu()
  {
	  switch(choice)
	  	  {
	  	  	  case 0:
	  	  		  drawMainMenu();
	  	  		  break;
	  	  	  case 1:
	  	  		  drawTempMenu();
	  	  		  break;
	  	  }
  }*/

  while (1)
  {
	  //BSP_LCD_Clear(LCD_COLOR_GREEN);
	  drawMainMenu();
	  BSP_TS_GetState(&TS_State);
	  if(TS_State.touchDetected)
	  {
	      x = TS_State.touchX[0];
	      y = TS_State.touchY[0];
	      if(abs(x-350)< 20 && abs(y-130) < 20)
		  {
			  relay ^= 1;
		  }
	      HAL_Delay(200);
	      //while(TS_State.touchDetected);
	  }


	  //HAL_Delay(200);

  }
}


static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 432;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /* activate the OverDrive to reach the 216 Mhz Frequency */
  if(HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    Error_Handler();
  }
}


static void Error_Handler(void)
{
  while(1)
  {
  }
}


static void MPU_Config(void)
{
  MPU_Region_InitTypeDef MPU_InitStruct;

  /* Disable the MPU */
  HAL_MPU_Disable();

  /* Configure the MPU attributes as WT for SRAM */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.BaseAddress = 0x20010000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_256KB;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_CACHEABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_SHAREABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER0;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.SubRegionDisable = 0x00;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);

  /* Enable the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}


static void CPU_CACHE_Enable(void)
{
  /* Enable I-Cache */
  SCB_EnableICache();

  /* Enable D-Cache */
  SCB_EnableDCache();
}

#ifdef  USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
  while (1)
  {
  }
}
#endif
